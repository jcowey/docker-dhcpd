FROM ubuntu:cosmic
MAINTAINER artem.silenkov@gmail.com
ENV DEBIAN_FRONTEND noninteractive
RUN rm -rf /var/lib/apt/lists/*
RUN echo "deb http://archive.ubuntu.com/ubuntu/ cosmic main universe" >> /etc/apt/sources.list
RUN apt-get update
RUN apt-get install -y libgcc1 libstdc++6 \
                       libmariadbclient18 libpq5 \
                       libssl1.1 zlib1g curl jq gnupg git procps \
                       kea-admin kea-dhcp-ddns-server kea-dhcp4-server kea-dhcp6-server kea-ctrl-agent

RUN apt-get -y dist-upgrade \
  && apt-get -y install net-tools tcpdump iptables \
  && cp -r /etc/kea /etc/kea.orig \
  && apt-get clean

ADD scripts/run.sh /

RUN apt-get clean

RUN apt-get install -y nodejs npm
RUN git clone https://github.com/isc-projects/kea-anterius.git /srv/anterius
COPY ./anterius_config.json /srv/anterius/config
WORKDIR /srv/anterius
RUN npm install

RUN mkdir -p /var/run/kea/ /var/kea/
COPY ./keactrl.conf /etc/kea/keactrl.conf
COPY ./kea-ca.conf /etc/kea/kea-ca.conf
COPY ./kea-ca.conf /etc/kea/kea-ctrl-agent.conf
EXPOSE 67 67/udp 547 547/udp 647 647/udp 847 847/udp 8686

WORKDIR /srv/anterius
CMD ["npm","start"]
ENTRYPOINT ["/run.sh"]
VOLUME ["/etc/kea","/var/lib/kea"]
